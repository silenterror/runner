# GoRunner

## This repository contains code that shows how to run tasks concurrently

### This project uses urls as a basis to get the data from the urls.
### It shows error handling on an error in the go routines.

#### Execution

```sh
go run main.go
```

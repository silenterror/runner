package main

import (
	"log"
	"os"

	"gitlab.com/silenterror/goroutine/runner"
)

var (
	// A list of urls to pull data and one bad url to show an error
	urls = []string{
		"https://newton.now.sh/api/v2/factor/x^2-1",
		"http://api.open-notify.org/astros.json",
		"https://ssd-api.jpl.nasa.gov/fireball.api?date-min=2022-01-01&req-loc=true",
		"http://api.foo.org/", // Bogus url to show completion of tasks with errors present
	}

	// all the results that were successful
	results = []string{}
	failed  = []error{}
)

func main() {
	wr := runner.New()
	wr.Run(urls)

	// exec the wait to wait for all go routines to finish
	// also closing the errChan channel as there are no more errs
	// this allows a non-blocking call so the errors can be
	// read off the error chan
	//	wr.Waiter()
	for {
		select {
		case done := <-wr.Done:
			if done {
				log.Println(results)
				os.Exit(0)
			}
		case result := <-wr.Completed:
			wr.Mutex.Lock()
			results = append(results, result)
			wr.Results = results
			wr.Mutex.Unlock()
		case errResult := <-wr.ErrChan:
			if errResult != nil {
				failed = append(failed, errResult)
				wr.Failed = failed
				log.Println("successful results: ", wr.Results)
				log.Println("failed urls", wr.Failed)
				os.Exit(1)
			}
		}
	}

}

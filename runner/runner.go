package runner

import (
	"bytes"
	"errors"
	"io/ioutil"
	"net/http"
	"sync"
)

// IRunner - the contract to implement for IRunner
type Runner interface {
	Run()
	Waiter()
}

// Runner - the type that can run tasks concurrently
type WebRunner struct {
	WaitGroup sync.WaitGroup
	ErrChan   chan error
	Completed chan string
	Failed    []error
	Mutex     sync.Mutex
	client    http.Client
	Results   []string
	Done      chan bool
}

// New - creates a new WebRunner
func New() *WebRunner {
	return &WebRunner{
		WaitGroup: sync.WaitGroup{},
		ErrChan:   make(chan error),
		Completed: make(chan string),
		Failed:    []error{},
		Mutex:     sync.Mutex{},
		client:    http.Client{},
		Results:   []string{},
		Done:      make(chan bool),
	}

}

// Run - execs the task in a go routine
func (w *WebRunner) Run(urls []string) {
	for _, v := range urls {
		w.WaitGroup.Add(1)
		go func(v string) {
			defer w.WaitGroup.Done()

			err := w.task(v)
			if err != nil {
				w.Mutex.Lock()
				w.Mutex.Unlock()
				w.ErrChan <- err
			}
		}(v)
	}
	w.Waiter()
}

// waiter - execs a go routine to wait for task completion and closes the err chan
func (w *WebRunner) Waiter() {
	go func() {
		w.WaitGroup.Wait()
		w.Done <- true
		close(w.Completed)
		close(w.ErrChan)
		close(w.Done)
	}()

}

// createRequest - creates an http request
func (w *WebRunner) createRequest(method string, url string, payload []byte) (*http.Request, error) {
	if method == http.MethodGet {
		request, err := http.NewRequest(http.MethodGet, url, nil)
		if err != nil {
			return nil, err
		}
		return request, nil
	}

	if method == http.MethodPost {
		request, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(payload))
		if err != nil {
			return nil, err
		}

		return request, nil

	}

	return nil, errors.New("error occurred creating the request")
}

// requester - makes the actual http call using the request
func (w *WebRunner) requester(request *http.Request) (string, error) {
	// call the graph service with the client using the request
	response, err := w.client.Do(request)
	if err != nil {
		return "", err
	}

	// check the status of the response if it is 200
	if response.StatusCode != http.StatusOK {
		return "", err
	}

	// reading the actual body of the response
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", err
	}

	// checking if the response is empty
	if len(string(body)) <= 0 {
		return "", errors.New("error - response was empty")
	}

	return string(body), nil

}

// task - makes a get request using the url if success it appends to results
// in reality this could do or call anything
func (w *WebRunner) task(url string) error {
	request, err := w.createRequest(http.MethodGet, url, nil)
	if err != nil {
		return err
	}

	response, err := w.requester(request)
	if err != nil {
		return err
	}

	w.Completed <- response

	return nil
}
